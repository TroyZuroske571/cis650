import sys
import time
import paho.mqtt.client as mqtt
from math import ceil
import random, decimal

if len(sys.argv) != 3:
    print("len is: " + str(len(sys.argv)))
    print('ERROR\nusage: ricart_agrawla.py <int: ID> <comma-separated ints: neighbors>. Sample usage: ricart_agrawla.py 1 \"2,3\"')
    sys.exit()

try:
    id = int(sys.argv[1])  # UID is the neighbor's ID (either 1 or 2)
except ValueError:
    print('ERROR2\nusage: ricart_agrawla.py <int: ID> <comma-separated ints: neighbors>. Sample usage: ricart_agrawla.py 1 \"2,3\"')
    sys.exit()

neighbors = []   # All node IDs

neighbors_str = str(sys.argv[2])
neighbors_str_split = neighbors_str.split(",")
for n in neighbors_str_split:
    neighbors.append(int(n))
    print("Adding neighbor: " + str(n))

clock = 1
pending = []
privileged = False
req_stamp = 0   # Stamp sent out when sending a request. 0 means no requests out.
counter = 0 # number of permissions I have obtained
cs_needed = True
request_string = 'r'
permission_string = 'p'

# Send request message in form of "r_{clock}_{id}"
def send_req_message(neighbor_id,clock,my_id):
    client.publish("rb_" + str(neighbor_id), request_string + "_" + str(clock) + "_" + str(my_id))

# Send permission message in form of "p_{id}"
def send_permission_message(neighbor_id, my_id):
    client.publish("rb_" + str(neighbor_id), permission_string + "_" + str(my_id))

# Process a received message
def process_message(msg):
    index = msg.index("_")  # Will throw exception if not found. This is probably ok?
    message_type = msg[:index]
    if (message_type == request_string):
        index2 = msg[index+1:].index("_") + (index+1)
        clock = msg[index + 1:index2]
        neighbor_id = msg[index2+1:]
        on_request(int(neighbor_id),int(clock))
    elif (message_type == permission_string):
        neighbor_id = msg[index + 1:]
        on_permission(int(neighbor_id))

def get_critical():
    global req_stamp    # Do we need these?
    global counter    # Do we need these?
    for n in neighbors:
        send_req_message(n,clock,id)
        req_stamp = clock   # Remember this value in case clock changes. For processing future requests.
        counter = 0

def on_permission(neighbor_id):
    global neighbors
    if (neighbor_id in neighbors):
        neighbors.remove(neighbor_id)
    else:
        print("Investigate this: " + str(neighbor_id))
    global clock
    global counter
    global privileged
    clock += 1
#    counter += 1
    if counter == len(neighbors):
        counter = 0
        privileged = True


def on_request(j,time):
    global neighbors
    global counter
    if (not j in neighbors):
        neighbors.append(j)
        counter += 1
    global clock
    if (time > clock):
        clock = time + 1
    else:
        clock += 1
    if (not privileged) and (req_stamp == 0 or (time,j) < (req_stamp,id)): #(time < req_stamp) or (time == req_stamp and j < id)):  # TODO: Does this tuple comparison work? (time,j) < (req_stamp,id)
        send_permission_message(j, id)
    else:
        pending.append(j)

def perform_critical(): # do work here in cs
    global privileged
    global pending
    global req_stamp
    print("Entering critical section")
    time.sleep(1) # Hang out in the critical section.
    print("Leaving critical section")
    privileged = False
    # Send messages out to queued processes
    for p in pending:
        send_permission_message(p, id)
    req_stamp = 0
    pending = []


##### MQTT Configuration ######
broker      = 'green0'
port        = 1883

# publish topics
neighbor_topics = []
for n in neighbors:
    neighbor_topics.append("rb_" + str(n))

# subscribe topics
my_topic = 'rb_' + str(id)
will_topic = 'will/'

#quality of service
qos = 0

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    temp = True # Do nothing.
#    print("Message ID "+str(mid)+ " successfully published")

# Called when a message is received by this node
def on_receive(client, userdata, msg):
    global got_vars
    got_vars = True
    process_message(msg.payload)
#    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)

#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    client = mqtt.Client(str(id), clean_session=True)

    # setup will for client
    will_message = "Dead UID: {}".format(id)
    client.will_set(will_topic, will_message)

    # callbacks
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_message = on_message

    # callbacks for specific topics
    client.message_callback_add(my_topic, on_receive)
    client.message_callback_add(will_topic, on_will)

    # connect to broker
    client.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    client.subscribe([(my_topic, qos),
                      (will_topic, qos),
                      ])

#    client.loop_forever()
    client.loop_start()    # Run one thread in background.

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    client.disconnect()

#TODO: Carvalho-Roucairol optimization: If someone hasn't bugged me since the last time I sent a request, I don't need to send a request to them.
#TODO: Is this a race condition? Can we make get_critical() atomic? We'll need to work this out.
#TODO: Might be ok (and worst case is that we don't enter in correct order.) Will need to test this.
#Error case: I'm node 1. I don't send a message to node 2, because node 2 hasn't needed CS since last time I entered.
# I send a message to node 3 asking for permission. While waiting, I get a request from node 2 with a lower time.
# I sent permission to node 1. Now I need to update my neighbors to say that I need permission from 2.
# Add code: At this point, I need to send a request to node 2.

time.sleep(5)

while True:
    # Do stuff outside of cs
    if cs_needed:
        get_critical()
        while (not privileged): # Do other stuff while waiting
            temp = 1
#            time.sleep(0.1)
        perform_critical()