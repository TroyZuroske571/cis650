"""
CIS 650
Kmeans w/ python's multiprocessing part 1
Implmented by: Troy Zuroske
"""
######### IMPORTS #########
import sys
import math
import os.path
import numpy as nump
from multiprocessing import Pool,current_process,cpu_count,active_children
from timeit import default_timer as timer
from functools import partial
from operator import add

args = sys.argv

if len(args) > 1:
    iterations = min( int(args[1]), 100 ) # change 100 if you want higher or lower
else:
    iterations = 10 #default

if len(args) > 2:
    processors = min( int(args[2]), cpu_count() )
else:
    processors = 1 #default

print('Total cores available: ' + str(cpu_count()))
print('Actual cores used: ' + str(processors))

# read 150K+ points into familiar form
def readFile(fname):
    fid = open(fname,'r')
    data = fid.readlines()
    #had to modify for formatting
    return map(lambda line : map(float, line.rstrip().split(',')),data) # remove newline and split on comma

# This function called when set up pool of processors. For now, just prints debugging info.
def start_process():
    print( 'Starting {} with pid {}'.format(current_process().name,current_process().pid)) #delayed print from when pool initialized
    return

# Read in data
dir = os.path.dirname(os.path.realpath(__file__))
fname = "2016_03_08_preprocessed.txt" #150K points, 2 dimensions of value and duration
batch = readFile(dir+"/"+fname) #one big batch
print('Total points: ' + str(len(batch))) # 155229

# Initialize each c in C. You can change both k and values if you see something better.
C = {1: [120, 0.0003], 2: [125,0.0001],3: [130,0.0001]} # k=3

# number of workers
N = processors


def euclidean_distance(vec1, vec2):
    zipped = zip(vec1, vec2)
    sqdiff = map(lambda pair: (pair[0] - pair[1])**2, zipped)
    summation = reduce(lambda a,b: a+b, sqdiff)
    return math.sqrt(summation)


def compute_center(point_vec, all_centers):
    distances = map(lambda pair: (pair, euclidean_distance(all_centers[pair], point_vec)), all_centers.keys())
    tup = reduce(lambda a, b: a if a[1] <= b[1] else b, distances)
    cid = tup[0] # cid
    return point_vec, cid


# map function to be threaded
def map_centers_to_points(all_centers, points):
    return compute_center(points, all_centers), 1


# seperate the centers (1, 2, and 3); need for part II
def seperate_centers(thread_list):
    new_dict = {}
    for all_points in thread_list:
        try:
            new_dict[all_points[0][1]].append(all_points)
        except KeyError:
            new_dict[all_points[0][1]] = [all_points]
    return new_dict


# calculate the new centers
def calc_new_centers(centers_to_calc):
    center = centers_to_calc[0][1]
    point = map(lambda pair: nump.array(pair[0][0]), centers_to_calc)
    the_count = map(lambda pair: pair[1], centers_to_calc)
    sum = reduce(add, point)
    divide_by = reduce(add, the_count)

    return center, list(sum/divide_by)

#may not need/use this
def check_empty(changed_centers, all_centers):
    dict3 = dict(all_centers, **changed_centers)
    return dict3

# Start a pool of N workers
pool = Pool(processes=N, initializer=start_process)

######### KMEANS #########

# Reminder: (1) map function takes 1 arg, (2) you may need to pass 2 args to map function, e.g., current value of C and points,
#           (3) you can get around the problem by using the partial function from functools,
#            e.g., # http://stackoverflow.com/questions/15331726/how-does-the-functools-partial-work-in-python.
#            See also https://en.wikipedia.org/wiki/Currying

total_time = 0
for i in range(iterations):
    print("========= Starting iteration " + str(i))

    start = timer()
    # your code goes here to find closest center for each point.

    p_assign = partial(map_centers_to_points, C)
    p_to_c_map = pool.map(p_assign, batch)

    end = timer()
    t = end - start
    total_time += t
    print( "time of MAP: " + str(t))
    seperate_points = seperate_centers(p_to_c_map)
    new_centers = list
    for key in seperate_points:
        temp = (calc_new_centers(seperate_points.get(key)))
        C[key] = temp[1]

    print('Current centers value: ' + str(C)) # just to make sure moving
  
pool.close() # no more tasks
pool.join()  # wrap up current tasks
print("Total time: " + str(total_time))
